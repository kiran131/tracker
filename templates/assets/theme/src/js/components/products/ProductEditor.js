export default class ProductEditor {
    constructor() {
        this.grid = $("#product--detail-modal");
        this.productModal = $("#productDetailModal");
        this.productRow = $(".product--item-row");
        this.detailPathElement = $("#detail--hidden-element");

        console.log("initialised");
        this.bindEvents();
    }

    bindEvents = () => {
        this.productRow.on("click",this.doAjaxCall)
    }

    doAjaxCall = (e) => {
        let productID = e.currentTarget.getAttribute("data-itemid");
        $.ajax({
            dataType: 'html',
            type: 'GET',
            data: {
                productID: productID,
            },
            url: this.getDetailPath(),
            success: this.appendItems
        });
    }

    getDetailPath = () =>{
        return this.detailPathElement.val();
    }

    appendItems = (response) => {
        try {
            let res;
            res = JSON.parse(response)

            if (res.success) {
                this.grid.empty();
                this.grid.html(res.data);
                this.productModal.modal.show();
            }else{
                console.log("false");
            }

        } catch (e) {
            console.log("AJAX API ERROR : " + e)
        }
    }
}