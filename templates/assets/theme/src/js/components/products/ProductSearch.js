export default class ProductSearch {
    constructor() {
        this.page = 1;
        this.grid = $('.items--grid');
        this.searchInput = $(".product-search-input");
        this.bindEvents()
    }

    bindEvents = () => {
        this.searchInput.on("keyup", this.doAjaxCall)
    }

    doAjaxCall = () => {
        $.ajax({
            dataType: 'html',
            type: 'GET',
            data: {
                key: this.getSearchKeywords(),
                page: this.getPage(),
                isAjax: true
            },
            url: this.getCurrentPagePath(),
            success: this.appendItems
        });
    }

    getSearchKeywords = () => {
        return encodeURI(this.searchInput.val());
    }

    getPage = () => {
        return this.page;
    }

    addPage = () => {
        this.page += 1;
    };

    resetPage = () => {
        this.page = 1;
    };

    getCurrentPagePath = () => {
        return location.href;
    };

    appendItems = (response) => {
        try {
            let res;
            res = JSON.parse(response)

            if (res.success) {
                this.grid.empty();
                this.grid.html(res.data)
            } else {
                this.grid.html("<div> NO RESULTS </div>")
            }

        } catch (e) {
            console.log("AJAX API ERROR")
        }

    }

}