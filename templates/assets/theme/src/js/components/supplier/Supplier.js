export default class Supplier {

    constructor() {
        this.supplierListDiv = $('.supplier-list');
        this.supplierRowItem = $('.supplier-item');
        this.supplierDetailForm = $('.supplier-detail');
        this.crsfToken = $("#csrf").attr('data-value');
        this.errorAlert = $('#error_alert')
        this.successAlert = $('#success_alert')
        this.addSupplierBtn = $('.add-supplier-btn')
        this.supplierListView = $('.list-view')
        this.supplierAddView = $('.add-form-view')
        this.supplierAddForm = $('#supplier_add')
        this.supplierSaveBtn = $('.supplier-btn-save')
        this.supplierSaveAddBtn = $('.supplier-btn-save-add')

        this.bindEvents();

    }

    bindEvents = () => {
        this.supplierRowItem.on('focusout', this.editRow)
        this.addSupplierBtn.on('click', this.showSupplierForm)
        this.supplierSaveBtn.on('click', this.supplierSave)
        this.supplierSaveAddBtn.on('click', this.supplierSaveAdd)
    }

    editRow = (e) => {
        let element = e.currentTarget

        let id = $(element).attr('data-supplier_id')
        let type = $(element).attr('data-var')
        let value = $(element).text()
        let url = $(element).attr('data-url')

        $.ajax({
            dataType: 'html',
            type: 'POST',
            headers: { 'X-CSRFToken': this.crsfToken },
            data: {
                id: id,
                type: type,
                value: value
            },
            url: url,
            success: (res) => {
                $(element).html(value)
                this.successAlert.show()

                setTimeout(() => {
                    this.successAlert.hide()
                }, 1000)
            },
            error: (xhr, req, res) => {
                this.errorAlert.show()
                setTimeout(() => {
                    this.errorAlert.hide()
                }, 1000)
            }
        });

    }

    showSupplierForm = (e) => {
        this.supplierListView.hide()
        this.supplierAddView.removeClass('invisible')
    }

    supplierSave = (e) => {
        e.preventDefault()
        this.addSupplier()
        location.reload()
    }

    supplierSaveAdd = (e) => {
        e.preventDefault()
        this.addSupplier()
        this.supplierAddForm.trigger('reset')
    }

    addSupplier = () => {
        let actionUrl = this.supplierAddForm.attr('action')

        $.ajax({
            type: "POST",
            headers: { 'X-CSRFToken': this.crsfToken },
            url: actionUrl,
            data: this.supplierAddForm.serialize(),
            success: (res) => {
                this.successAlert.show()

                setTimeout(() => {
                    this.successAlert.hide()
                }, 1000)
            },
            error: (xhr, req, res) => {
                this.errorAlert.show()
                setTimeout(() => {
                    this.errorAlert.hide()
                }, 1000)
            }
        });
    }

}