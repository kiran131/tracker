export default class BillingCart {
    constructor() {
        this.itemRow = $('.product--item-row');
        this.bindEvents();
    }

    bindEvents = () => {
        this.itemRow.on('click',this.itemAdd)
    }

    itemAdd = (e) => {
        let productID = e.currentTarget.getAttribute('data-productID');
        let apiLink   = e.currentTarget.getAttribute('data-apiLink');
        $.ajax({
            dataType: 'html',
            type: 'GET',
            data: {
                productID: productID
            },
            url: apiLink,
            success: function(res){
                console.log(res);
            }
        });
    }
}