import BillingCart from "./components/billing/BillingCart";
import BillingProductSearch from "./components/billing/BillingProductSearch";
import ProductSearch from "./components/products/ProductSearch";
import Supplier from "./components/supplier/Supplier";
import App from "./react";

class app {
    constructor() {
        this.init();
    }

    init =()=> {
        if($(".product--search").length > 0) {
            new ProductSearch();
        }
        if($(".billing-product--search").length > 0) {
            new BillingProductSearch();
            new BillingCart();
        }

        if($('.supplier-list')) {
            new Supplier()
        }

        // Initialise react app
        new App();
    }
}

$(document).ready(function () {
    new app();
})
