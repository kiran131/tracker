
// REACT CODE

import React from 'react';
import ReactDOM from 'react-dom/client';
import Product from "./react-components/product";
import SupplierAdd from './react-components/supplier/supplier-add';

export default class App {

    constructor() {
        this.init();
    }

    init = () => {

        if ($('#productdom').length > 0){
            this.productPage()
        }
        
    }

    productPage = () => {
        this.productElement = document.getElementById("productpage")
        this.root = ReactDOM.createRoot(this.productElement)

        this.root.render(<Product/>)
    }

}