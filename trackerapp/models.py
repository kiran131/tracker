from django.db import models


class User(models.Model):
    name       = models.CharField(max_length=50)
    email      = models.CharField(max_length=50)
    password   = models.CharField(max_length=300)
    type       = models.CharField(max_length=100,default="admin")

    def __str__(self):
        return self.name


class Product(models.Model):
    product_code        = models.CharField(max_length=200, default=None)
    product_category    = models.CharField(max_length=200, default=None)
    product_name        = models.CharField(max_length=200, default=None)
    product_hsn         = models.CharField(max_length=200, default=0)
    product_base_price  = models.CharField(max_length=200, default=0)
    product_mrp         = models.CharField(max_length=200, default=0)
    product_tax         = models.CharField(max_length=200, default=0)
    product_disc_price  = models.CharField(max_length=200, default=0)
    is_disabled         = models.BooleanField(default=False)
    is_deleted          = models.BooleanField(default=False)

    def __str__(self):
        return self.product_code + " : " + self.product_name + " - " + self.product_mrp + "/-"


class Vendor(models.Model):
    ven_name        = models.CharField(max_length=200, default=None)
    ven_addr        = models.CharField(max_length=500, default=None)
    ven_phone       = models.CharField(max_length=100, default=None)
    ven_gst         = models.CharField(max_length=100, default=None)
    place_of_supply = models.CharField(max_length=100, default=None)


    def __str__(self):
        return self.ven_name


class Stock(models.Model):
    product_id    = models.ForeignKey(Product, on_delete=models.DO_NOTHING, default=0)
    stock_added   = models.DateField(auto_now=True)
    stock_updated = models.DateField(auto_now=True)
    unit_type     = models.CharField(max_length=200, default="PCS")
    units         = models.IntegerField(default=0)
    vendor_id     = models.ForeignKey(Vendor, default=None, on_delete=models.DO_NOTHING)

class Customer(models.Model):
    customer_name  = models.CharField(max_length=200, default='')
    customer_phone = models.CharField(max_length=20, default=None)

    
class Invoice(models.Model):

    invoiceTypes = (
        ("Q","Quick Sale"),
        ("N","Non Billed"),
        ("I","Invoiced")
    )

    invoice_no            = models.CharField(max_length=200,default='0000')
    invoice_type          = models.CharField(max_length=50,choices=invoiceTypes)
    invoice_date          = models.DateField(auto_now=True)
    invoice_disc          = models.FloatField(default=0)
    invoice_total         = models.FloatField(default=0)
    invoice_tax           = models.FloatField(default=0)
    invoice_customer_name = models.CharField(max_length=200,default='')
    invoice_customer_ph   = models.CharField(max_length=15, default='')
    invoice_adv_pay       = models.FloatField(default=0)
    invoice_bal_pay       = models.FloatField(default=0)

class InvoiceItems(models.Model):
    
    invoice_id = models.ForeignKey(Invoice, on_delete=models.DO_NOTHING)
    product_id = models.ForeignKey(Product, on_delete=models.DO_NOTHING)
    item_qty   = models.IntegerField(default=0)
    

