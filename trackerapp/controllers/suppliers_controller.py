import sys
import urllib
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.template import loader
from django.http.response import JsonResponse
from django.template.loader import render_to_string
from django.core.paginator import Paginator
from django.template.loader import get_template
from django.db import IntegrityError


# Models Import Lines
from trackerapp.models import *

def supplier_list(req):
    page_number = req.GET.get('page')
    keywords    = urllib.parse.unquote(req.GET.get('key')) if req.GET.get('key') else ""
    is_ajax      = req.GET.get('is_ajax') or False

    suppliers = Vendor.objects.all()

    paginator = Paginator(suppliers, 50)

    data = {
        'page_number' : page_number,
        'keywords' : keywords,
        'suppliers': paginator.get_page(page_number),
        'supplier' : suppliers[0]
    }
    if is_ajax:
        rendered = render_to_string('elements/page_elements/suppliers/suppliers_list.html', data)
        return JsonResponse({
            'success' : True,
            'data' : rendered
        })
    else:
        return render(req, "pages/suppliers/suppliers.html", data)


def supplier_delete(req,s_id):
    try:
        Vendor.objects.get(id=s_id).delete()
    except IntegrityError:
        req.session['errors'] = "Already attached Supplier to inventory cannot delete!"
        req.session.modified = True

        return redirect("suppliers")
        
    return redirect('suppliers')


def supplier_add(req):

    try: 

        vendor = Vendor()

        vendor.ven_name        = req.POST.get("ven_name") or "" 
        vendor.ven_addr        = req.POST.get("ven_addr") or "" 
        vendor.ven_phone       = req.POST.get("ven_phone") or ""  
        vendor.ven_gst         = req.POST.get("ven_gst") or ""
        vendor.place_of_supply = req.POST.get("place_of_supply") or ""

        vendor.save()
    except:
        return JsonResponse({
            'success' : False,
            'message' : "{} {}".format(sys.exc_info()[0],sys.exc_info()[1])
        }, status=500)

    return JsonResponse({
            'success' : True,
            'message' : 'Added!'
        },status=200)


def supplier_update_attr(req):
    try:
        supplier_id = req.POST.get("id")
        attr_type = req.POST.get('type')
        attr_value = req.POST.get('value')

        supplier = Vendor.objects.get(id=supplier_id)
        setattr(supplier, attr_type, attr_value)
        supplier.save()

    except:
        return JsonResponse({
            'success' : False,
            'message' : "{} {}".format(sys.exc_info()[0],sys.exc_info()[1])
        }, status=500)

    return JsonResponse({
            'success' : True,
            'message' : 'Updated!'
        },status=200)
    