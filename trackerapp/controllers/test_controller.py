from django.http import HttpResponse
from django.shortcuts import render
from django.template import loader


from trackerapp.models import *

def importer(req):
    file_path = "/home/kiran/Documents/UPDATED AMUL LIST.csv"

    with open(file_path, 'r') as fh:

        for counter, line in enumerate(fh, 1):
            row = line.split(",")
            product = Product()
            product.product_code = row[1]
            product.product_category = row[2]
            product.product_name = row[3]
            product.product_mrp = row[5]
            product.product_base_price = row[7]
            product.product_tax = row[8]
            product.product_disc_price = row[10]
            product.save()

def reseter(req):
    product = Product()
    product.objects.all().delete()