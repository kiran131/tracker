from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.template import loader
from django.http.response import JsonResponse
from django.template.loader import render_to_string
from django.core.paginator import Paginator
from django.template.loader import get_template
from django.contrib.auth import authenticate, login

from trackerapp.models import *



def login_page(req):
    if req.user.is_authenticated:
        return redirect("/dashboard")
    return render(req, "pages/login.html")


def do_login(req):
    name = req.POST.get("name")
    passwd = req.POST.get("pass")

    user = authenticate(req, username=name, password=passwd)
    if user is not None:
        login(req, user)
        return redirect("/dashboard")
    else:
        return render(req, "pages/login.html")


def vendor_page(req):
    return render(req, "pages/dashboard.html")


def dashboard_page(req):
    return render(req, "pages/dashboard.html")


def billing_page(req):
    return render(req, "pages/billing.html")


def register(res):
    pass



def custom_view(request):
    context = {
        "content": "Dummy",
        "content_about": "about dummy"
    }
    body_template = loader.get_template('pages/site_list.html')

    return HttpResponse(body_template.render(context, request))
