# Essentials import

import urllib
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.template import loader
from django.http.response import JsonResponse
from django.template.loader import render_to_string
from django.core.paginator import Paginator
from django.template.loader import get_template


# Models Import Lines
from trackerapp.models import *
from trackerapp.utillity import product_utility


# Controller functions
def product_lisitng_page(req):
    page_number = req.GET.get('page')
    keywords    = urllib.parse.unquote(req.GET.get('key')) if req.GET.get('key') else ""
    is_ajax      = req.GET.get('is_ajax') if req.GET.get('is_ajax') else False

    products = product_utility.product_search(keywords)
    product_count = Product.objects.all().filter(is_disabled=False,is_deleted=False).count()

    paginator = Paginator(products, 50)

    data = {
        'page_number' : page_number,
        'keywords' : keywords,
        'products': paginator.get_page(page_number),
        'product_count' : product_count
    }
    if is_ajax:
        rendered = render_to_string('elements/page_elements/products/product_items.html', data)
        return JsonResponse({
            'success' : True,
            'data' : rendered
        })
    else:
        return render(req, "pages/products/products.html", data)


def product_detail(req, p_id=None):
    if p_id == None :
        product_id = req.GET.get("product_id")
    else:
        product_id = p_id
    is_ajax    = req.GET.get('is_ajax') if req.GET.get('is_ajax') else False

    if product_id is None:
         return JsonResponse({
                'success' : False,
                'data' : 'No Data'
            })
    else:
        product = Product.objects.get(id=product_id)
        data = {
            "product" : product,
            "prev_url" : req.META.get('HTTP_REFERER')
        }

        rendered = render_to_string('elements/page_elements/products/product_detail_modal.html', data)

        if is_ajax: 
            return JsonResponse({
                "success" : True,
                "data" : rendered
            })
        else:
            return render(req, "pages/products/product_detail.html", data)


def product_update(req,p_id=None):
    if p_id == None :
        product_id = req.GET.get("product_id")
    else:
        product_id = p_id

    is_ajax             = req.GET.get('is_ajax') if req.GET.get('is_ajax') else False
    product_name        = req.GET.get("product_name")
    product_category    = req.GET.get("product_category")
    product_base_price  = req.GET.get("product_base_price")
    product_disc_price  = req.GET.get("product_disc_price")
    product_mrp         = req.GET.get("product_mrp")
    product_tax         = req.GET.get("product_tax")

    product = Product.objects.get(id=product_id)

    if not product:
        return redirect('products')
    else: 

        if product_name is not None:
            product.product_name = product_name
        if product_category is not None:
            product.product_category = product_category
        if product_base_price is not None:
            product.product_base_price = product_base_price
        if product_disc_price is not None:
            product.product_disc_price = product_disc_price
        if product_mrp is not None:
            product.product_mrp = product_mrp
        if product_tax is not None:
            product_tax = float(product_tax.replace("%", ""))
            product.product_tax = product_tax
        
        product.save()
        product.refresh_from_db()
    
        return redirect('product-detail',p_id=product_id)


def product_delete(req,p_id=None):
    if p_id == None :
        product_id = req.GET.get("product_id")
    else:
        product_id = p_id
    is_ajax    = req.GET.get('is_ajax') if req.GET.get('is_ajax') else False

    if product_id is None:
         return JsonResponse({
                'success' : False,
                'data' : 'No Data'
            })
    else:
        try:
            product = Product.objects.get(id=product_id)
        except:
            return redirect('products')
        
        if not product.is_deleted:
            product.is_deleted = True
        else:
            product.is_deleted = False
        product.save()
        
        data = {
            "product" : product
        }

        return redirect('products')


def product_disable(req,p_id=None):
    if p_id == None :
        product_id = req.GET.get("product_id")
    else:
        product_id = p_id
    is_ajax    = req.GET.get('is_ajax') if req.GET.get('is_ajax') else False

    if product_id is None:
         return JsonResponse({
                'success' : False,
                'data' : 'No Data'
            })
    else:
        product = Product.objects.get(id=product_id)
        if not product.is_disabled:
            product.is_disabled = True
        else:
            product.is_disabled = False
        product.save()

        print(product.is_disabled)
        
        data = {
            "product" : product
        }

        return redirect('product-detail',p_id=product_id)

def product_add(req):
    return render(req, "pages/products/product_add.html")


def product_add_action(req):
    product = Product()

    product.product_name       = req.GET.get("product_name")
    product.product_category   = req.GET.get("product_category")
    product.product_base_price = req.GET.get("product_base_price")
    product.product_disc_price = req.GET.get("product_disc_price")
    product.product_mrp        = req.GET.get("product_mrp")
    product.product_tax        = req.GET.get("product_tax")

    product.save()

    return redirect("products")