from django.http import HttpResponse
from django.shortcuts import render
from django.template import loader
from django.http.response import JsonResponse
from django.template.loader import render_to_string
from django.core.paginator import Paginator
from django.template.loader import get_template

from trackerapp.models import *

def inventory_listing_page(req):
    inventory = Stock.objects.all()
    data = {
        'inventory': inventory
    }
    return render(req, "pages/inventory/inventory.html", data)

def inventory_update(req, pID):

    req.session['purchase_items'] = {
        
    }

    inventory = Stock.objects.all()
    data = {
        'inventory': inventory,
        'session' : req.session['purchase_items']
    }
    return render(req, "pages/inventory/inventory_update.html", data)

def inventory_update_action(req):
    
    is_ajax          = req.GET.get('is_ajax') if req.GET.get('is_ajax') else False
    unit_type        = req.GET.get('unit_type')
    units            = req.GET.get('units') 
    product_id       = req.GET.get("product_id")
    vendor_id        = req.GET.get("vendor_id")

    if 'is_update_request' is not None:
        product = Product.objects.get(id=product_id)
        vendor  = Vendor.objects.get(id=vendor_id)

def inventoryProductSearch(req):
    pass

def inventoryDefaultProductListBestSelling(req):
    pass