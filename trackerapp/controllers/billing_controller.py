from django.http import HttpResponse
from django.shortcuts import render
from django.template import loader
from django.http.response import JsonResponse
from django.template.loader import render_to_string
from django.core.paginator import Paginator
from django.template.loader import get_template
import urllib.parse

from trackerapp.models import *
from trackerapp.utillity import product_utility

def billing_page(req):
    invoice_cart = {}
    products = Product.objects.all().filter(is_disabled=False,is_deleted=False).order_by("product_category")[:15];
    data = {
        'products' : products
    }
    return render(req, "pages/billing/billing.html",data)

def product_search(req):
    keywords    = urllib.parse.unquote(req.GET.get('key')) if req.GET.get('key') else ""
    is_ajax      = req.GET.get('is_ajax') if req.GET.get('is_ajax') else False

    products = product_utility.product_search(keywords)
    data = {
        'products' : products[:15]
    }

    rendered = render_to_string('elements/page_elements/inventory/inventory_search_items.html', data)
    return JsonResponse({
            'success' : True,
            'data' : rendered
        })

def product_add(req):
    product_id      = req.GET.get('product_id') if req.GET.get('product_id') else False

    product_ob = Product.objects.get(id=product_id)
    billing_cart_products = []

    req.session['products'] = billing_cart_products.append(product_ob)
    return JsonResponse({
                "success" : True
            })

