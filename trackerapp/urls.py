from django.urls import path

from . import views
from .controllers import tracker_controller
from .controllers import product_controller
from .controllers import inventory_controller
from .controllers import suppliers_controller
from .controllers import test_controller
from .controllers import billing_controller

productURLS = [
    path("dashboard/products", product_controller.product_lisitng_page, name='products'),
    path("dashboard/product-detail/<int:p_id>", product_controller.product_detail, name='product-detail'),
    path("dashboard/product-add/", product_controller.product_add, name='product-add'),
    path("dashboard/product-add-action/", product_controller.product_add_action, name='product-add-action'),
    path("dashboard/product-disable/<int:p_id>", product_controller.product_disable, name='product-disable'),
    path("dashboard/product-delete/<int:p_id>", product_controller.product_delete, name='product-delete'),
    path("dashboard/product-update/<int:p_id>", product_controller.product_update, name='product-update'),
]

inventoryURLS = [
    path("dashboard/inventory", inventory_controller.inventory_listing_page, name='inventory'),
    path("dashboard/inventory-update/<int:p_id>", inventory_controller.inventory_update, name='inventory-update'),
]

supplierURLS = [
    path("dashboard/suppliers", suppliers_controller.supplier_list, name='suppliers'),
    path("dashboard/supplier-delete/<int:s_id>", suppliers_controller.supplier_delete, name='supplier-delete'),
    path("dashboard/supplier-add/", suppliers_controller.supplier_add, name='supplier-add'),
    path("dashboard/supplier-update-attr/", suppliers_controller.supplier_update_attr, name='supplier-update-attr'),
]

billingURLS = [
    path("dashboard/billing", billing_controller.billing_page, name='billing'),
    path("dashboard/billing/product-search", billing_controller.product_search, name='billing-product-search'),
    path("dashboard/billing/product-add", billing_controller.product_add, name='billing-product-add'),
]

urlpatterns = [
    path("", views.index, name='index'),
    path("login", tracker_controller.login_page, name='login'),
    path("login-action", tracker_controller.do_login, name='loginaction'),
    path("dashboard", tracker_controller.dashboard_page, name='dashboard'),
    path("dashboard/import", test_controller.importer, name='importer'),
    path("dashboard/reseter", test_controller.reseter, name='reseter'),
]

urlpatterns += productURLS 
urlpatterns += inventoryURLS 
urlpatterns += billingURLS
urlpatterns += supplierURLS