from django.contrib import admin

from .models import User
from .models import Product
from .models import Stock
from .models import Vendor

admin.site.register(User)
admin.site.register(Product)
admin.site.register(Stock)
admin.site.register(Vendor)