#Generic File for the common methods used.

# Models Import Lines
from trackerapp.models import *

def product_search(keywords):
    if not keywords:
        products = Product.objects.all().filter(is_disabled=False,is_deleted=False).order_by("product_category")
    else:
        products = Product.objects.filter(is_disabled=False,is_deleted=False,product_category__icontains=keywords).order_by("product_category")
        if products.count() < 1:
            products = Product.objects.filter(is_disabled=False,is_deleted=False,product_name__icontains=keywords).order_by("product_category")

    return products