# Generated by Django 4.1 on 2022-08-06 05:08

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Customer',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('customer_name', models.CharField(default='', max_length=200)),
                ('customer_phone', models.CharField(default=None, max_length=20)),
            ],
        ),
        migrations.CreateModel(
            name='Invoice',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('invoice_no', models.CharField(default='0000', max_length=200)),
                ('invoice_type', models.CharField(choices=[('Q', 'Quick Sale'), ('N', 'Non Billed'), ('I', 'Invoiced')], max_length=50)),
                ('invoice_date', models.DateField(auto_now=True)),
                ('invoice_disc', models.FloatField(default=0)),
                ('invoice_total', models.FloatField(default=0)),
                ('invoice_tax', models.FloatField(default=0)),
                ('invoice_customer_name', models.CharField(default='', max_length=200)),
                ('invoice_customer_ph', models.CharField(default='', max_length=15)),
                ('invoice_adv_pay', models.FloatField(default=0)),
                ('invoice_bal_pay', models.FloatField(default=0)),
            ],
        ),
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('product_code', models.CharField(default=None, max_length=200)),
                ('product_category', models.CharField(default=None, max_length=200)),
                ('product_name', models.CharField(default=None, max_length=200)),
                ('product_base_price', models.CharField(default=0, max_length=200)),
                ('product_mrp', models.CharField(default=0, max_length=200)),
                ('product_tax_percent', models.CharField(default=0, max_length=200)),
                ('product_disc_price', models.CharField(default=0, max_length=200)),
                ('is_disabled', models.BooleanField(default=False)),
                ('is_deleted', models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
                ('email', models.CharField(max_length=50)),
                ('password', models.CharField(max_length=300)),
                ('type', models.CharField(default='admin', max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='Vendor',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('ven_name', models.CharField(default=None, max_length=200)),
                ('ven_addr', models.CharField(default=None, max_length=500)),
                ('ven_phone', models.CharField(default=None, max_length=15)),
                ('ven_gst', models.CharField(default=None, max_length=100)),
                ('place_of_supply', models.CharField(default=None, max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='Stock',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('stock_added', models.DateField(auto_now=True)),
                ('stock_updated', models.DateField(auto_now=True)),
                ('unit_type', models.CharField(default='PCS', max_length=200)),
                ('units', models.IntegerField(default=0)),
                ('product_id', models.ForeignKey(default=0, on_delete=django.db.models.deletion.DO_NOTHING, to='trackerapp.product')),
                ('vendor_id', models.ForeignKey(default=None, on_delete=django.db.models.deletion.DO_NOTHING, to='trackerapp.vendor')),
            ],
        ),
        migrations.CreateModel(
            name='InvoiceItems',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('item_qty', models.IntegerField(default=0)),
                ('invoice_id', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='trackerapp.invoice')),
                ('product_id', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='trackerapp.product')),
            ],
        ),
    ]
